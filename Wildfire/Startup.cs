﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Wildfire.Startup))]
namespace Wildfire
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
